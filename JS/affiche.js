/**
 * @author Johnny Tsheke --UQAM
 */

var universites=new Array("UQAM","ETS", "UQAC","UQO","UQTR");

function afficheR()
{
	var nbelem=universites.length;
	var trie=universites.slice(0);
	trie.sort();
	var nbelem=trie.length;
	var mesg="<ol>";
	for(var i=0;i<nbelem;i++)
	{
		mesg=mesg+" <li>"+trie[i]+"</li>";
	}
	mesg=mesg+" </ol>";
	document.getElementById("carreR").innerHTML=mesg;
}

function afficheV()
{
	var mesg="<ul>";
	for(uni in universites)
	{
		mesg=mesg+" <li>"+universites[uni]+"</li>";
	}
	mesg=mesg+" </ul>";
	document.getElementById("carreV").innerHTML=mesg;
}

function afficheB()
{
	var nb=universites.length;
	var mesg="Au total "+nb+" universités enregistrées <br>";
	document.getElementById("carreB").innerHTML=mesg;
}

function effaceR()
{
	document.getElementById("carreR").innerHTML=null;
}

function greveR()
{
document.getElementById("carreR").innerHTML="ON EST EN GREVE!";	
}

function effaceV()
{
	document.getElementById("carreV").innerHTML=null;
}
 function effaceB()
 {
 	document.getElementById("carreB").innerHTML=null;
 }
